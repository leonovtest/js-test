const puppeteer = require('puppeteer');

module.exports= async function(){
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: {
            width: 1920,
            height: 1080
        },
        args: ['--window-size=1920,1080']
    })


    const page =  await browser.newPage()
    return page

}

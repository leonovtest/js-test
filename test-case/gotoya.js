module.exports = async function(page) {
    await page.goto('https://ya.ru')
    await page.focus('#text')
    await page.keyboard.type('market.yandex.ru')
    await page.click('body > table > tbody > tr.b-table__row.layout__search > td > form > div.search2__button > button')

    await page.mainFrame().waitForSelector('body > div.main.serp.i-bem.main_js_inited.serp_js_inited > div.main__center > div.main__content > div.content.i-bem.content_js_inited > div.content__left > ul > li')
    let count=await page.evaluate(function(){
        return document.querySelectorAll('body > div.main.serp.i-bem.main_js_inited.serp_js_inited > div.main__center > div.main__content > div.content.i-bem.content_js_inited > div.content__left > ul > li').length
    })
    return count

}